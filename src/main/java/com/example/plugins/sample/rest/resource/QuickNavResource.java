/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.plugins.sample.rest.resource;

import com.atlassian.confluence.search.actions.json.ContentNameMatch;
import com.atlassian.confluence.search.actions.json.ContentNameSearchResult;
import com.atlassian.confluence.search.contentnames.ContentNameSearchService;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.atlassian.confluence.user.UserAccessor;
import com.atlassian.plugins.rest.common.security.AnonymousAllowed;
import com.atlassian.user.Group;
import java.util.List;
import javax.servlet.http.HttpServletRequest;
import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.Response;
import com.atlassian.spring.container.ContainerManager;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author TheTT
 */
@Path("/search")
public class QuickNavResource {

    // <editor-fold defaultstate="collapsed" desc="parameter">
    private final ContentNameSearchService contentNameSearchService;
    private static final String CONFLUENCE_GROUP_PREFIX = "confluence-";
    private static final String WORKFLOW_GROUP_PREFIX = "workflow-";
    private static final String OR_CONDITION = " OR ";
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="public method">
    public QuickNavResource(ContentNameSearchService contentNameSearchService) {
        this.contentNameSearchService = contentNameSearchService;
    }

    @GET
    @Produces({"application/json"})
    @AnonymousAllowed
    public Response getQuickNavResults(@Context HttpServletRequest httpServletRequest, @QueryParam("query") String query, @QueryParam("type") List<String> types, @QueryParam("spaceKey") String spaceKey, @QueryParam("maxPerCategory") Integer maxPerCategoryNullableDefault) {
        int maxPerCat = maxPerCategoryNullableDefault != null ? maxPerCategoryNullableDefault : -1;
        ConfluenceUser confluenceUser = AuthenticatedUserThreadLocal.get();
        UserAccessor userAccessor = (UserAccessor) ContainerManager.getComponent("userAccessor");
        Map<String, String> mapValidUsers = this.getValidUsers(confluenceUser, userAccessor);

        ContentNameSearchResult search = this.contentNameSearchService.search(query, types, spaceKey, maxPerCat, httpServletRequest);

        for (List<ContentNameMatch> lstContentNameMatch : search.getContentNameMatches()) {
            for (ContentNameMatch contentNameMatch : lstContentNameMatch) {
                if (contentNameMatch.getClassName().equals("content-type-userinfo") && !mapValidUsers.containsKey(contentNameMatch.getName())) {
                    lstContentNameMatch.remove(contentNameMatch);
                }
            }
        }
        return Response.ok(search).build();
    }


    public Map<String, String> getValidUsers(ConfluenceUser confluenceUser, UserAccessor userAccessor) {

        Map<String, String> mapListUser = new HashMap<String, String>();
        mapListUser.put(confluenceUser.getName(), confluenceUser.getName());
        List<Group> listGroups = userAccessor.getGroupsAsList(confluenceUser);
        for (Group group : listGroups) {
            if (group.getName().contains(CONFLUENCE_GROUP_PREFIX) || group.getName().contains(WORKFLOW_GROUP_PREFIX)) {
                continue;
            }
            List<String> listUsers = userAccessor.getMemberNamesAsList(group);
            for (String user : listUsers) {
                if (!mapListUser.containsKey(user)) {
                    mapListUser.put(user, user);
                }
            }
        }
        return mapListUser;
    }
    // </editor-fold>
}
