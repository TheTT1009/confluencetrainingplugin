/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.plugins.sample.macro;

import com.atlassian.confluence.content.render.xhtml.ConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultConversionContext;
import com.atlassian.confluence.content.render.xhtml.DefaultXmlEventReaderFactory;
import com.atlassian.confluence.content.render.xhtml.Renderer;
import com.atlassian.confluence.content.render.xhtml.XmlOutputFactoryFactoryBean;
import com.atlassian.confluence.macro.Macro;
import com.atlassian.confluence.macro.MacroExecutionException;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.renderer.PageContext;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.xml.HTMLParagraphStripper;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import org.springframework.beans.factory.annotation.Autowired;

/**
 *
 * @author TheTT
 */
public class MacroPlugin implements Macro {

    private SpaceManager spaceManager;
    private PageManager pageManager;
    private HTMLParagraphStripper htmlParagraphStripper;
    private Map<String, String> listUpdateMacroDuplicate;
    private Renderer viewRenderer;

    @Autowired
    public MacroPlugin() {
        listUpdateMacroDuplicate = new HashMap<String, String>();
        XMLOutputFactory xmlOutputFactory;
        try {
            xmlOutputFactory = (XMLOutputFactory) new XmlOutputFactoryFactoryBean(true).getObject();
        } catch (Exception e) {
            throw new RuntimeException("Error occurred trying to construct a XML output factory", e);
        }
        this.htmlParagraphStripper = new HTMLParagraphStripper(xmlOutputFactory, new DefaultXmlEventReaderFactory());
    }

    @Override
    public String execute(Map<String, String> map, String string, ConversionContext context) throws MacroExecutionException {

        if (map.get("pageName") == null) {
            return "No page";
        } else {
            Page page = findPagebyName(map.get("pageName"));

            String bodyResult = "";
            try {
                PageContext pageContext = new PageContext(page, context.getPageContext());
                pageContext.setOutputType(context.getOutputType());
                DefaultConversionContext defaultConversionContext = new DefaultConversionContext(pageContext);
                defaultConversionContext.setContentTree(context.getContentTree());

                String strippedBody = this.htmlParagraphStripper.stripFirstParagraph(page.getBodyAsString());
                bodyResult = this.getViewRenderer().render(strippedBody, defaultConversionContext);
            } catch (XMLStreamException ex) {
                Logger.getLogger(MacroPlugin.class.getName()).log(Level.SEVERE, null, ex);
            }

            return bodyResult;
        }
    }

    @Override
    public BodyType getBodyType() {
        return Macro.BodyType.NONE;
    }

    @Override
    public OutputType getOutputType() {
        return Macro.OutputType.BLOCK;
    }

    /**
     *
     * @param name
     * @return find Page by input name
     */
    public Page findPagebyName(String name) {

        List<Page> allPages = new ArrayList<Page>();
        List<Space> allSpaceList = spaceManager.getAllSpaces();
        for (Space space : allSpaceList) {
            allPages.addAll(pageManager.getPages(space, true));
        }

        for (Page page : allPages) {
            if (page.getDisplayTitle().matches(name)) {
                return page;
            }
        }
        return null;
    }

    /**
     * @return the spaceManager
     */
    public SpaceManager getSpaceManager() {
        return spaceManager;
    }

    /**
     * @param spaceManager the spaceManager to set
     */
    public void setSpaceManager(SpaceManager spaceManager) {
        this.spaceManager = spaceManager;
    }

    /**
     * @return the pageManager
     */
    public PageManager getPageManager() {
        return pageManager;
    }

    /**
     * @param pageManager the pageManager to set
     */
    public void setPageManager(PageManager pageManager) {
        this.pageManager = pageManager;
    }

    /**
     * @return the htmlParagraphStripper
     */
    public HTMLParagraphStripper getHtmlParagraphStripper() {
        return htmlParagraphStripper;
    }

    /**
     * @param htmlParagraphStripper the htmlParagraphStripper to set
     */
    public void setHtmlParagraphStripper(HTMLParagraphStripper htmlParagraphStripper) {
        this.htmlParagraphStripper = htmlParagraphStripper;
    }

    /**
     * @return the listUpdateMacroDuplicate
     */
    public Map<String, String> getListUpdateMacroDuplicate() {
        return listUpdateMacroDuplicate;
    }

    /**
     * @param listUpdateMacroDuplicate the listUpdateMacroDuplicate to set
     */
    public void setListUpdateMacroDuplicate(Map<String, String> listUpdateMacroDuplicate) {
        this.listUpdateMacroDuplicate = listUpdateMacroDuplicate;
    }

    /**
     * @return the viewRenderer
     */
    public Renderer getViewRenderer() {
        return viewRenderer;
    }

    /**
     * @param viewRenderer the viewRenderer to set
     */
    public void setViewRenderer(Renderer viewRenderer) {
        this.viewRenderer = viewRenderer;
    }
}
