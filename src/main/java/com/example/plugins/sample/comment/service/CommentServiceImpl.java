/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.plugins.sample.comment.service;

import com.example.plugins.sample.comment.entity.CommentEntity;
import com.atlassian.activeobjects.external.ActiveObjects;
import static com.google.common.base.Preconditions.checkNotNull;
import static com.google.common.collect.Lists.newArrayList;
import java.util.List;
import net.java.ao.Query;

/**
 *
 * @author TheTT
 */
public class CommentServiceImpl implements CommentService {

    // <editor-fold defaultstate="collapsed" desc="parameters">
    private final ActiveObjects ao;

    public CommentServiceImpl(ActiveObjects ao) {
        this.ao = checkNotNull(ao);
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="public method">
    @Override
    public CommentEntity add(Long id, String comment, String time) {
        final CommentEntity comm = ao.create(CommentEntity.class);
        comm.setPageId(id);
        comm.setTime(time);
        comm.setComment(comment);
        comm.save();
        return comm;
    }

    @Override
    public List<CommentEntity> getCommentById(Long id) {
        return newArrayList(ao.find(CommentEntity.class, Query.select().where("PAGE_ID = ?", id)));
    }
    // </editor-fold>
}
