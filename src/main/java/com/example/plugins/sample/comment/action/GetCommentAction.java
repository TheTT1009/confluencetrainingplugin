/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.plugins.sample.comment.action;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.example.plugins.sample.comment.service.CommentService;
import com.example.plugins.sample.helper.ActionContextHelper;
import com.example.plugins.sample.comment.entity.CommentEntity;
import com.opensymphony.xwork.ActionContext;
import java.util.List;
import com.atlassian.confluence.core.Beanable;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author TheTT
 */
public class GetCommentAction extends ConfluenceActionSupport implements Beanable {

    // <editor-fold defaultstate="collapsed" desc="parameters">  
    private CommentService commentService;
    private ActionContextHelper actionHelper;
    private String result;
    private PageManager pageManager;
    private Page page;
    // </editor-fold>
    
    // <editor-fold defaultstate="collapsed" desc="public method">
    @Override
    public Object getBean() {
        
        Map<String, Object> bean = new HashMap<String, Object>();
        bean.put("lstComment", this.result);
        return bean;
    }

    @Override
    public String execute() throws Exception {
        
        Long pageId;
        ActionContext context = ActionContext.getContext();
        pageId = Long.parseLong(actionHelper.getParameterValue(context, "pageId"));
        page = pageManager.getPage(pageId);
        this.result = getCommentList(pageId);
        return SUCCESS;
    }

    /**
     * @param pageId
     * @return return comment list of page as json
     */
    public String getCommentList(Long pageId) {

        //create list to store comments of page
        List<CommentEntity> cmtList;
        cmtList = commentService.getCommentById(pageId);

        //use StrinBuilder to create show comments popup
        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append("<section id='comment-popup' class='aui-dialog2 aui-dialog2-large aui-layer' role='dialog'  aria-hidden='true'>");
        strBuilder.append("<header class='aui-dialog2-header'>");
        strBuilder.append("<h2 class='aui-dialog2-header-main'>");
        strBuilder.append(page.getTitle());
        strBuilder.append("</h2>");
        strBuilder.append("</header>");
        strBuilder.append("<div class='aui-dialog2-content'>");

        strBuilder.append("<table class='aui aui-table-sortable' style='padding-top: 15px' id='page-list-table'>");
        strBuilder.append("<thead>");
        strBuilder.append("<tr>");
        strBuilder.append("<th id='page-name'>Content</th>");
        strBuilder.append("<th id='id-title'>Time</th>");
        strBuilder.append("</tr>");
        strBuilder.append("</thead>");
        strBuilder.append("<tbody>");

        //loop to show information of each comment in list
        for (CommentEntity commentEntity : cmtList) {
            strBuilder.append("<tr>");
            strBuilder.append("<td>");
            strBuilder.append(commentEntity.getComment());
            strBuilder.append("</td>");
            strBuilder.append("<td>");
            strBuilder.append(commentEntity.getTime());
            strBuilder.append("</td>");
            strBuilder.append("</tr>");
        }

        strBuilder.append("</tbody>");
        strBuilder.append("</table>");

        strBuilder.append("</div>");
        strBuilder.append("<footer class='aui-dialog2-footer'>");
        strBuilder.append("<div class='aui-dialog2-footer-actions'>");
        strBuilder.append("<button id='comment-popup-cancel' class='aui-button aui-button-link'>");
        strBuilder.append("Cancel");
        strBuilder.append("</button>");
        strBuilder.append("</div>");
        strBuilder.append("</footer>");
        strBuilder.append("</section>");
        return strBuilder.toString();
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="geter-seter">
    /**
     * @return the commentService
     */
    public CommentService getCommentService() {
        return commentService;
    }

    /**
     * @return the result
     */
    public String getResult() {
        return result;
    }

    /**
     * @param result the result to set
     */
    public void setResult(String result) {
        this.result = result;
    }

    /**
     * @param commentService the commentService to set
     */
    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }

    /**
     * @return the pageManager
     */
    public PageManager getPageManager() {
        return pageManager;
    }

    /**
     * @param pageManager the pageManager to set
     */
    public void setPageManager(PageManager pageManager) {
        this.pageManager = pageManager;
    }

    /**
     * @return the page
     */
    public Page getPage() {
        return page;
    }

    /**
     * @param page the page to set
     */
    public void setPage(Page page) {
        this.page = page;
    }
    // </editor-fold>
}
