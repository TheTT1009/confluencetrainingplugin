/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.plugins.sample.comment.entity;

import net.java.ao.Entity;
import net.java.ao.Preload;

/**
 *
 * @author TheTT
 */
@Preload
public interface CommentEntity extends Entity {

    String getComment();

    void setComment(String comment);

    String getTime();

    void setTime(String time);

    Long getPageId();

    void setPageId(Long id);
}
