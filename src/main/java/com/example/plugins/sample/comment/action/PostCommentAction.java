/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.plugins.sample.comment.action;

import com.atlassian.confluence.core.Beanable;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.example.plugins.sample.comment.service.CommentService;
import com.example.plugins.sample.helper.ActionContextHelper;
import com.opensymphony.xwork.ActionContext;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author TheTT
 */
public class PostCommentAction extends ConfluenceActionSupport implements Beanable{

    // <editor-fold defaultstate="collapsed" desc="parameter">  
    private CommentService commentService;
    private boolean result;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="public method">
    @Override
    public Object getBean() {

        Map<String, Object> bean = new HashMap<String, Object>();
        bean.put("result", this.result);
        return bean;
    }
    
    @Override
    public String execute() throws Exception {

        String comment;
        String time;
        Long pageId;
        ActionContext context = ActionContext.getContext();
        time = getCurrentTime();
        pageId = Long.parseLong(ActionContextHelper.getParameterValue(context, "pageId"));
        comment = ActionContextHelper.getParameterValue(context, "comment");
        commentService.add(pageId, comment, time);
        result = true;
        return SUCCESS;
    }

    /**
     * @return get time of creating comment
    */
    public String getCurrentTime() {
        DateFormat sdf = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
        Date date = new Date();
        String CurrentTime = sdf.format(date);
        return CurrentTime;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="getter setter">  
    /**
     * @return the commentService
     */
    public CommentService getCommentService() {
        return commentService;
    }

    /**
     * @param commentService the commentService to set
     */
    public void setCommentService(CommentService commentService) {
        this.commentService = commentService;
    }
    // </editor-fold>
    
}
