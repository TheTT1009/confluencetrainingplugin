/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.plugins.sample.comment.service;
import com.atlassian.activeobjects.tx.Transactional;
import com.example.plugins.sample.comment.entity.CommentEntity;
import java.util.List;

/**
 *
 * @author TheTT
 */
@Transactional
public interface CommentService {

    CommentEntity add(Long id, String comment, String time);

    List<CommentEntity> getCommentById(Long id);

}
