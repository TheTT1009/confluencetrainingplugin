/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.plugins.sample.helper;

import com.atlassian.confluence.pages.Page;
import com.opensymphony.xwork.ActionContext;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author TheTT
 */
public class ActionContextHelper {

    // <editor-fold defaultstate="collapsed" desc="public method">  
    public static String getParameterValue(ActionContext context, String key) {

        Object paramValues = context.getParameters().get(key);
        if (paramValues instanceof String[] && ((String[]) paramValues).length != 0) {
            return ((String[]) paramValues)[0];
        } else if (paramValues instanceof String) {
            return (String) paramValues;
        }
        return null;
    }

    public static List<Page> getPageTreeFromHome(Page rootPage) {
        List<Page> result = new ArrayList<Page>();
        if (rootPage == null) {
            return result;
        }
        List<Page> pages = rootPage.getSortedChildren();
        if (!rootPage.isHomePage()) {
            result.add(rootPage);
        }
        for (Page p : pages) {
            result.addAll(getPageTreeFromHome(p));
        }
        return result;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="private method">
    private ActionContextHelper() {
    }
    // </editor-fold>
}
