/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.plugins.sample.basic.action;

import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.atlassian.confluence.user.AuthenticatedUserThreadLocal;
import com.atlassian.confluence.user.ConfluenceUser;
import com.opensymphony.xwork.ActionContext;
import com.atlassian.user.UserManager;
import com.example.plugins.sample.helper.ActionContextHelper;
import java.util.List;

/**
 *
 * @author TheTT
 */
public class CreateSpaceAction extends ConfluenceActionSupport{
    
    // <editor-fold defaultstate="collapsed" desc="parameter">
    private SpaceManager spaceManager;
    private UserManager userManager;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="public method">
    @Override
    public String execute() {
        
        String spaceName;
        String spaceKey;
        String description;
        ActionContext context = ActionContext.getContext();
        spaceName = ActionContextHelper.getParameterValue(context, "spaceName");
        spaceKey = ActionContextHelper.getParameterValue(context, "spaceKey");
        description = ActionContextHelper.getParameterValue(context, "des");
        ConfluenceUser user = AuthenticatedUserThreadLocal.get();
        
        //validate entered Space Name and Space Key
        if (validSpaceName(spaceName) == false || validSpaceKey(spaceKey) == false) {
            return ERROR;
        } else {
            spaceManager.createSpace(spaceKey, spaceName, description, user);
            return SUCCESS;
        }
    }
    
    /**
     * @return validate entered Space Name
    */
    public Boolean validSpaceName(String name) {

        List<Space> spaceList = spaceManager.getAllSpaces();
        for (Space space : spaceList) {
            String existName = space.getName();
            if (name.equals(existName)) {
                return false;
            }
        }
        return true;
    }
    
    /**
     * @return validate entered Space Key
    */
    public Boolean validSpaceKey(String key) {

        List<Space> spaceList = spaceManager.getAllSpaces();
        for (Space space : spaceList) {
            String existKey = space.getKey();
            if (key.equalsIgnoreCase(existKey)) {
                return false;
            }
        }
        return true;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="getter setter">
    public UserManager getUserManager() {
        return userManager;
    }

    public void setUserManager(UserManager userManager) {
        this.userManager = userManager;
    }

    public SpaceManager getSpacemanager() {
        return spaceManager;
    }

    public void setSpacemanager(SpaceManager spacemanager) {
        this.spaceManager = spacemanager;
    }
    // </editor-fold>
}
