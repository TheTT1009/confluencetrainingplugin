/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.plugins.sample.basic.action;

import com.atlassian.confluence.core.Beanable;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import com.opensymphony.xwork.ActionContext;
import java.util.List;

import com.example.plugins.sample.helper.ActionContextHelper;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author TheTT
 */
public class ShowPagesAction extends ConfluenceActionSupport implements Beanable {

    // <editor-fold defaultstate="collapsed" desc="parameter">
    private PageManager pageManager;
    private SpaceManager spaceManager;
    private String result;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="public method">
    public Object getBean() {

        Map<String, Object> bean = new HashMap<String, Object>();
        bean.put("pageList", this.result);
        return bean;
    }

    @Override
    public String execute() throws Exception {

        String spaceKey;
        ActionContext context = ActionContext.getContext();
        spaceKey = ActionContextHelper.getParameterValue(context, "spaceKey");
        this.result = getListPage(spaceKey);
        return SUCCESS;
    }

    public String getListPage(String spaceKey) {

        Space space = spaceManager.getSpace(spaceKey);
        Page homePage = space.getHomePage();
        List<Page> listChildPages = ActionContextHelper.getPageTreeFromHome(homePage);
        
        String baseUrl = settingsManager.getGlobalSettings().getBaseUrl();

        StringBuilder strBuild = new StringBuilder();
        strBuild.append("<br/><br/>");

        strBuild.append("<table class='aui dataTable' style='padding-top: 15px' id='page-list-table'>");
//        strBuild.append("<table id='page-list-table'>");
        strBuild.append("<thead>");
        strBuild.append("<tr>");
        strBuild.append("<th id='page-id'>Page Id</th>");
        strBuild.append("<th id='id-title'>Page Name</th>");
        strBuild.append("<th></th>");
        strBuild.append("<th></th>");
        strBuild.append("</tr>");
        strBuild.append("</thead>");
        strBuild.append("<tbody>");

        for (Page page : listChildPages) {
            strBuild.append("<tr>");
            strBuild.append("<td class='page-id' style='width: 15%'>");
            strBuild.append("<a href='");
            strBuild.append(baseUrl);
            strBuild.append(page.getUrlPath());
            strBuild.append("'>");
            strBuild.append(page.getId());
            strBuild.append("</a>");
            strBuild.append("</td>");
            strBuild.append("<td  style='width: 45%'>");
            strBuild.append("<a id='page-name' data-pageid='");
            strBuild.append(page.getId());
            strBuild.append("'class='page-name'>");
            strBuild.append(page.getTitle());
            strBuild.append("</a></td>");
            strBuild.append("<td>");
            strBuild.append("<a data-pageid='");
            strBuild.append(page.getId());
            strBuild.append("' data-pagename='");
            strBuild.append(page.getTitle());
            strBuild.append("'class='comment' id='btnAddComment'>");
            strBuild.append("Add comment");
            strBuild.append("</a></td>");
            strBuild.append("<td>");
            strBuild.append("<a data-pageid='");
            strBuild.append(page.getId());
            strBuild.append("'class='comment-show'>");
            strBuild.append("Show Comment");
            strBuild.append("</a></td>");
            strBuild.append("</tr>");
        }

        strBuild.append("</tbody>");
        strBuild.append("<tfoot>");
        strBuild.append("</tfoot>");
        strBuild.append("</table>");
        return strBuild.toString();
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="getter setter">
    public SpaceManager getSpaceManager() {
        return spaceManager;
    }

    public void setSpaceManager(SpaceManager spaceManager) {
        this.spaceManager = spaceManager;
    }

    public PageManager getPageManager() {
        return pageManager;
    }

    public void setPageManager(PageManager pageManager) {
        this.pageManager = pageManager;
    }
// </editor-fold>
}
