/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.example.plugins.sample.basic.action;

import com.atlassian.confluence.core.Beanable;
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.pages.Attachment;
import com.atlassian.confluence.pages.AttachmentManager;
import com.atlassian.confluence.pages.Page;
import com.atlassian.confluence.pages.PageManager;
import com.opensymphony.xwork.ActionContext;
import java.util.List;

import com.example.plugins.sample.helper.ActionContextHelper;
import java.util.HashMap;
import java.util.Map;

/**
 *
 * @author TheTT
 */
public class GetAttachmentAction extends ConfluenceActionSupport implements Beanable {

    // <editor-fold defaultstate="collapsed" desc="parameter">
    private AttachmentManager attachmentManager;
    private PageManager pageManager;
    private String result;
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="public method">
    @Override
    public Object getBean() {

        Map<String, Object> bean = new HashMap<String, Object>();
        bean.put("lstAttachment", this.result);
        return bean;
    }

    @Override
    public String execute() throws Exception {

        long pageId;
        ActionContext context = ActionContext.getContext();
        pageId = Long.parseLong(ActionContextHelper.getParameterValue(context, "pageId"));
        result = getAttachList(pageId);
        return SUCCESS;
    }

    public String getAttachList(Long id) {

        Page page;
        page = pageManager.getPage(id);
        List<Attachment> attachmentList = attachmentManager.getAttachments(page);

        StringBuilder strBuilder = new StringBuilder();
        strBuilder.append("<section id='attachment-popup' class='aui-dialog2 aui-dialog2-large aui-layer attachment-popup' role='dialog' aria-hidden='true'>");
        strBuilder.append("<header class='aui-dialog2-header'>");
        strBuilder.append("<h2 class='aui-dialog2-header-main'>");
        strBuilder.append(page.getTitle());
        strBuilder.append("</h2>");
        strBuilder.append("</header>");
        strBuilder.append("<div class='aui-dialog2-content'>");

        strBuilder.append("<table class='aui aui-table-sortable' style='padding-top: 15px' id='page-list-table'>");
        strBuilder.append("<thead>");
        strBuilder.append("<tr>");
        strBuilder.append("<th id='page-name'>Name</th>");
        strBuilder.append("<th id='id-title'>Type</th>");
        strBuilder.append("</tr>");
        strBuilder.append("</thead>");
        strBuilder.append("<tbody>");

        for (Attachment attachment : attachmentList) {
            strBuilder.append("<tr>");
            strBuilder.append("<td>");
            strBuilder.append(attachment.getDisplayTitle());
            strBuilder.append("</td>");
            strBuilder.append("<td>");
            strBuilder.append(attachment.getType());
            strBuilder.append("</td>");
            strBuilder.append("</tr>");
        }

        strBuilder.append("</tbody>");
        strBuilder.append("</table>");

        strBuilder.append("</div>");
        strBuilder.append("<footer class='aui-dialog2-footer'>");
        strBuilder.append("<div class='aui-dialog2-footer-actions'>");
        strBuilder.append("<button id='attachment-cancel' class='aui-button aui-button-link attachment-cancel'>");
        strBuilder.append("Cancel");
        strBuilder.append("</button>");
        strBuilder.append("</div>");
        strBuilder.append("</footer>");
        strBuilder.append("</section>");
        return strBuilder.toString();
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="getter setter">
    public PageManager getPageManager() {
        return pageManager;
    }

    public void setPageManager(PageManager pageManager) {
        this.pageManager = pageManager;
    }

    public AttachmentManager getAttachmentManager() {
        return attachmentManager;
    }

    public void setAttachmentManager(AttachmentManager attachmentManager) {
        this.attachmentManager = attachmentManager;
    }

    /**
     * @return the result
     */
    public String getResult() {
        return result;
    }

    /**
     * @param result the result to set
     */
    public void setResult(String result) {
        this.result = result;
    }
    // </editor-fold>
}
