package com.example.plugins.sample.basic.action;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import com.atlassian.confluence.core.ConfluenceActionSupport;
import com.atlassian.confluence.labels.Label;
import com.atlassian.confluence.labels.SpaceLabelManager;
import com.atlassian.confluence.spaces.Space;
import com.atlassian.confluence.spaces.SpaceManager;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 *
 * @author TheTT
 */
public class ShowSpaceAction extends ConfluenceActionSupport {

    // <editor-fold defaultstate="collapsed" desc="parameter">
    private SpaceManager spaceManager;
    private List labelList;
    private SpaceLabelManager spaceLabelManager;
    private List<Space> spaceList = new ArrayList<Space>();
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="public method">
    @Override
    public String execute() throws Exception {

        this.spaceList = this.spaceManager.getAllSpaces();
        Iterator<Space> itr = spaceList.iterator();
        while (itr.hasNext()) {
            Space space = itr.next();
            labelList = spaceLabelManager.getLabelsOnSpace(space);
        }
        return SUCCESS;
    }
    // </editor-fold>

    // <editor-fold defaultstate="collapsed" desc="getter setter">
    public List getLabelList() {
        return labelList;
    }

    public void setLabelList(List labelList) {
        this.labelList = labelList;
    }

    /**
     * @return the spaceManager
     */
    public SpaceManager getSpaceManager() {
        return spaceManager;
    }

    /**
     * @param spaceManager the spaceManager to set
     */
    public void setSpaceManager(SpaceManager spaceManager) {
        this.spaceManager = spaceManager;
    }

    /**
     * @return the spaceLabelManager
     */
    public SpaceLabelManager getSpaceLabelManager() {
        return spaceLabelManager;
    }

    /**
     * @param spaceLabelManager the spaceLabelManager to set
     */
    public void setSpaceLabelManager(SpaceLabelManager spaceLabelManager) {
        this.spaceLabelManager = spaceLabelManager;
    }

    /**
     * @return the spaceList
     */
    public List<Space> getSpaceList() {
        return spaceList;
    }

    /**
     * @param spaceList the spaceList to set
     */
    public void setSpaceList(List<Space> spaceList) {
        this.spaceList = spaceList;
    }
    // </editor-fold>
}
