Confluence.QuickNav = (function ($) {
    var dropDownPostProcess = [];
    var makeParams;

    return {
        addDropDownPostProcess: function (dp) {
            if (typeof dp !== "undefined") {
                dropDownPostProcess.push(dp);
            } else {
                AJS.log("WARN: Attempted to add a dropdown post-process function that was undefined.");
            }
        },
        setMakeParams: function (mp) {
            makeParams = mp;
        },
        init: function (quickSearchInputField, dropDownPlacement) {
            quickSearchInputField.quicksearch("/rest/quicknav1/1/search", null, {
                dropdownPlacement: dropDownPlacement,
                dropdownPostprocess: function (dd) {
                    $.each(dropDownPostProcess, function (index, postProcessFunction) {
                        postProcessFunction && postProcessFunction(dd);
                    });
                },
                makeParams: function (value) {
                    if (makeParams)
                        return makeParams(value);
                    else
                        return {query: value};
                },
                ajsDropDownOptions: {
                    className: "quick-search-dropdown"
                }
            });
        }
    };
})(AJS.$);


