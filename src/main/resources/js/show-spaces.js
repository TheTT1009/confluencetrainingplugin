$(document).ready(function () {

    //declare jQuery for datatable to avoid conflict
    var jquery112 = $.noConflict(true);

    //ajax for showpage action
    $(document).on("click", ".space-name", function () {
        var url = "./showpages.action";
        var spaceKey = $(this).data('spacekey');
        $.ajax({
            url: url,
            type: "POST",
            dataType: "json",
            async: true,
            data: {spaceKey: spaceKey},
            success: function (data) {
                $("#list-page").html(data.pageList);
                //Js for paginate show page table
                jquery112('#page-list-table').DataTable({
                    "paginate": true,
                    "filter": false
                });
            },
            error: function (xhr) {
                console.log('Error ' + xhr.responseText);
            }
        });
    });

    //ajax for show attachment action
    $(document).on("click", ".page-name", function (e) {
        var url = "./getattachment.action";
        var pageId = $(this).data('pageid');
        $.ajax({
            url: url,
            type: "POST",
            dataType: "json",
            async: true,
            data: {pageId: pageId},
            success: function (data) {
                $("#list-attachment").html(data.lstAttachment);
                e.preventDefault();
                AJS.dialog2("#attachment-popup").show();

            },
            error: function (xhr) {
                console.log('Error ' + xhr.responseText);
            }
        });

    });

    //ajax for show comment action
    $(document).on("click", ".comment-show", function (e) {
        var url = "./getcomment.action";
        var Id = $(this).data('pageid');
        $.ajax({
            url: url,
            type: "POST",
            dataType: "json",
            async: true,
            data: {pageId: Id},
            success: function (data) {
                $("#list-comment").html(data.lstComment);
                e.preventDefault();
                AJS.dialog2("#comment-popup").show();
            },
            error: function (xhr) {
                console.log('Error ' + xhr.responseText);
            }
        });
    });

    $(document).on("click", "#comment-confirm", function () {
        var url = "./postcomment.action";
        var comment = $("#comment-text").val();
        var pageid = $("#page-id").val();
        $.ajax({
            url: url,
            type: 'POST',
            async: false,
            data: {pageId: pageid,
                comment: comment},
            success: function (data) {
                AJS.dialog2("#comment-dialog").hide();
                $("#comment-text").val("");
                if (data.result === true){
                    AJS.messages.success({
                       title: 'Congratulation',
                       body: 'Your comment have been posted.'
                    });
                } else {
                    AJS.messages.error({
                       title: 'Error',
                       body: 'Your comment cant be posted.'
                    });
                }
                
            },
            error: function (xhr) {
                console.log('Error ' + xhr.responseText);
            }
        });
    });

    AJS.$(document).on("click", ".attachment-cancel", function (e) {
        e.preventDefault();
        AJS.dialog2(".attachment-popup").hide();
        $("#attachment-popup").remove();
    });

    //js for comment popup
    AJS.$(document).on("click", ".comment", function (e) {
        e.preventDefault();
        //Lay 'pageid' tu <a>Add comment</a>
        var id = $(this).data('pageid');
        //Gan vao hidden input tren popup post comment id="page-id"
        $("#page-id").val(id);

        AJS.dialog2("#comment-dialog").show();
        var name = $(this).data('pagename');
        $("#title").html(name);
    });
    AJS.$(document).on("click", "#comment-cancel", function (e) {
        e.preventDefault();
        AJS.dialog2("#comment-dialog").hide();
    });

    AJS.$(document).on("click", "#comment-popup-cancel", function (e) {
        e.preventDefault();
        AJS.dialog2("#comment-popup").hide();
        $("#comment-popup").remove();
    });

    //js for createspace popup
    AJS.$(document).on("click", "#goto-createspace-button", function (e) {
        e.preventDefault();
        AJS.dialog2("#createspace-popup").show();
    });
    AJS.$(document).on("click", "#createspace-cancel", function (e) {
        e.preventDefault();
        AJS.dialog2("#createspace-popup").hide();
    });
});

